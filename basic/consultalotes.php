<?php

use yii\db\Query;

$query=(new Query())
->from('user')
->orderBy('id');

foreach ($query->batch() as $user)
{
    //$user is an array of 100 or fewer rows from the user table
}

foreach ($query->each() as $user)
{
    //$user representa uno fila de datos de la tabla user
}