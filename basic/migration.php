<?php

use yii\db\Schema;
use yii\db\Migration;

class m150101_185401_create_news_tables extends Migration
{
    public function up ()
    {
        $this->createTable('news', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
        ]);
    }

    public function  down()
    {   
        $this->dropTable('news');
        
        echo "m101129_185401_create_news_table cannot be reverted.\n";

        return false;
    }
      /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}