<?php 

namespace app\components;

use yii\web\UrlRuleInterface;
use yii\base\BaseObject;

class CarUrRule extends BaseObject implements UrlRulesInterface
{

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'car/index')
        {
            if(isset($params['manufacturer'],$params['model']))
            {
                return $params['manufacturer'].'/'.$params['model'];
            }elseif(isset($params['manufacture'])){
                return $params['manufacturer'];
            }
        }
        return false; //no se aplica esta regla
    }
    public function parseRequest($manager, $request)
{
    $pathInfo = $request->getPathInfo();
    if (preg_match('%^(\w+)(/(\w+))?$%', $pathInfo, $matches)) 
    {
        /*
    Comprueba $matches[1] y $matches[3] para ver
    si coincide con un  manufacturer y un model en la base de datos
    si coincide, establecer $params['manufacturer'] y/o $params['model']
    y devuelve ['car/index',$params]
        */
    }
    return false;//no se aplica la regla 
}
}