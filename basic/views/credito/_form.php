<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Convenio;
use app\models\Empresa;
use app\models\Credito;
use app\models\Clientes;

/* @var $this yii\web\View */
/* @var $model app\models\Credito */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="credito-form">
    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'cliente_id')->dropDownList(
        arrayHelper::map(Clientes::find()->where(['estatus'=>1])->all(),'id','nombre'),
        [ 'prompt'=>'Seleccione ID',]); ?>

    <?= $form->field($model, 'convenio_id')->dropDownList(
       arrayHelper::map(Convenio::find()->where(['estatus'=>1])->all(),'id','nombre'),
       [ 'prompt'=>'Seleccione Convenio']); ?>

    <?= $form->field($model, 'folio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'monto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descuento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_pagar')->textInput(['maxlength' => true])->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fecha_alta')->textInput()->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'fecha_actualizacion')->textInput()->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
