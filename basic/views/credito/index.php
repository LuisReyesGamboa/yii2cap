<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CreditoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Creditos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credito-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Credito', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cliente_id',
            'convenio_id',
            'folio',
            'monto',
            //'plazos',
            //'peropdicidad',
            //'tasa',
            //'descuento',
            //'total_pagar',
            //'fecha_alta',
            //'fecha_actualizacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
