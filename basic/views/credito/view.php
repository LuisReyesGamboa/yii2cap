<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Convenio;
use app\models\Credito;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Credito */


$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Creditos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


\yii\web\YiiAsset::register($this);
?>

<div class="credito-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes'=>[
            'id',
            'cliente_id',
            'convenio_id',
            'folio',
            'monto',
            'descuento',
            'total_pagar',
            //'fecha_alta',
            //'fecha_actualizacion',
        ],
    ]);
 ?>

<?= DetailView::widget([
        'model' => $convenio,
        'attributes'=>[
            'periodicidad',
            'plazos',
            'tasa',
            ],
    ]);
 ?>

        
</div>
