<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreditoPago */

$this->title = 'Update Credito Pago: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Credito Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="credito-pago-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
