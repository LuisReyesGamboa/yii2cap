<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreditoPago */

$this->title = 'Create Credito Pago';
$this->params['breadcrumbs'][] = ['label' => 'Credito Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credito-pago-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
