<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CreditoPagoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Credito Pagos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credito-pago-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Credito Pago', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'credito_id',
            'numero_pago',
            'monto',
            'fecha_pago',
            //'estatus',
            //'fecha_alta',
            //'fecha_actualizacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
