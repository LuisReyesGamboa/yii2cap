<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form= ACtiveForm::begin(['id'=>'login-form']);?>

<?= $form->field($model,'username')?>

<?= $form->field($model,'password')->passwordInput()?>

<div class="form-group">
    <?= Html::submitButton('Login')?>
</div>

<?php ActiveForm::end(); ?>
