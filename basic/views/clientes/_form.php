<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_paterno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_materno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_nacimiento')->widget(
        DatePicker::className(),[
        'dateFormat' => 'yyyy-MM-dd',
        'clientOptions' => [
            'maxDate'=>'-18Y',
            'yearRange' => '-110:-18',
            'changeYear' => true],
            'options' => ['class' => 'form-control', 'style' => 'width:100%']
    ]) ?>

    <?= $form->field($model, 'rfc')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->isNewRecord): ?>
    <?= $form->field($model, 'estatus')->radioList([1=>'activo',0=>'inactivo'])?>
    <?php endif; ?>
    
    <?= $form->field($model, 'fecha_alta')->textInput()->hiddenInput()->label(false)?>
 
    <?= $form->field($model, 'fecha_actualizacion')->textInput()->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
