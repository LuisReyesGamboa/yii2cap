<?php   

use yii\helpers\Html;

?>

<p>Tienes que registrar la siguiente informacion.</p>

<ul>
    <li><label>Nombre</label>: <?= Html::encode($model->name)?></li>
    <li><label>Email</label>. <?= Html::encode($model->email)?></li>
</ul>
