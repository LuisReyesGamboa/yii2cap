<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\html;

class HelloWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
        if($this->message === null)
        { $this->message = 'Hello World';}
    }
    public function run()
    {
        return Html::encode($this->message);
    }
}


//Widget con el contenido insertado entre las 
//llamadas a begin y end

/*
class HelloWidget extends Widget
{
    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        $content=ob_get_clean();
        return Html::encode($content);
    }
}
*/
