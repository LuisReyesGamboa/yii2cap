<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    
    <?php if(!$model->isNewRecord): ?>
    <?= $form->field($model, 'estatus')->radioList(['1'=>'Activo','0'=>'Inactivo']) ?>
    <?php endif; ?>

    <?= $form->field($model, 'fecha_alta')->textInput()->hiddenInput()->label(false)?>

    <?= $form->field($model, 'fecha_actualizacion')->textInput()->hiddenInput()->label(false)?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
