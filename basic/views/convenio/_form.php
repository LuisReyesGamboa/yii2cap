<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Empresa;
use app\models\Convenio;


/* @var $this yii\web\View */
/* @var $model app\models\Convenio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="convenio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'empresa_id')->dropDownList(
        arrayHelper::map(Empresa::find()->where(['estatus'=>1])->all(),'id','nombre'),
        [ 'prompt'=>'Seleccione Id'
    ]); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'periodicidad')->dropDownList(['1'=>'Mensual','2'=>'Quincenal','3'=>'Semanal'],
    ['prompt'=>'Elige opcion',
    'onchange'=>'$.post("index.php?r=convenio/get-periodicidad&id='.'"+$(this).val(),
    function( data){$("select#convenio-plazos").html(data);});']); ?>

    <?= $form->field($model, 'plazos')->dropDownList(
     [],
    [ 'prompt'=>'Seleccione plazo']) ?>


    <?= $form->field($model, 'tasa')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
    <?= $form->field($model, 'estatus')->radioList(['1'=>'Activo','0'=>'Inactivo']) ?>
    <?php endif; ?>

    <?= $form->field($model, 'fecha_alta')->textInput()->hiddenInput()->label(false)?>

    <?= $form->field($model, 'fecha_actualizacion')->textInput()->hiddenInput()->label(false)?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
