<?php

use yii\db\Migration;

class m150101_185401_create_news_table extends Migration
{
    public function up()
    {
        $this->createTable('news',[
            'id'=>$this->primaryKey(),
            'title'=>$this->string()->notNull(),
            'content'=>$this->tect(),
        ]);
    }

    public function down()
    {
        $this->dropTable('news');
    }
}