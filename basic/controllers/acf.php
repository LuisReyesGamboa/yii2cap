<?php

use yii\web\Controller;
use yii\filters\AccessControl;

class SiteController extends Controller 
{
    public function behaviors()
    {
        return [
            'access'=>[
                'class'=>AccessControl::className(),
                'only'=>['login','loguot','signup'],
                'rules'=>['allow'=>true,
                           'sction'=>['login','signup'],
                           'roles'=>['?'],
            ],
            [
                'allow'=>true,
                'action'=>['logout'],
                'roles'=>['@'],
            ],
        ],
    ];
    }
}