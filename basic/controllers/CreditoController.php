<?php

namespace app\controllers;

use Yii;
use app\models\Credito;
use app\models\CreditoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Convenio;
use app\models\Clientes;
use yii\helpers\ArrayHelper;

/**
 * CreditoController implements the CRUD actions for Credito model.
 */
class CreditoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Credito models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CreditoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Credito model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $conv=Convenio::findOne($id);
       /* \yii\helpers\VarDumper::dump($conv, 10, true);
          die();*/
        return $this->render('view',  [
            'model' => $this->findModel($id),
            'convenio' => $conv,
            ]);
        }

    /**
     * Creates a new Credito model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Credito();
       
        if($model->load(Yii::$app->request->post()))
    {    
        $con=Yii::$app->request->post();

       $conid=$con['Credito']['convenio_id'];
       $conv=Convenio::findOne($conid);
       $periodo=$conv->periodicidad;
       $plazo=$conv->plazos;
       $tasa=$conv->tasa;
       $porcentaje=($tasa/100);
       $monto=$model->monto;
       switch ($periodo)
       {
           case 1:
               $ciclos=12;
               break;
           case 2:
               $ciclos=24;
               break;
           case 3:
               $ciclos=52;
               break;
       }
       $totalfinal= $monto+$monto*$porcentaje*($plazo/$ciclos);
       $model->total_pagar=$totalfinal;

       if( $model->save()) {
        return $this->redirect(['view', 'id' => $model->id,  'convenio' => $conv,]);
        }     
   }    
    
            
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Credito model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()))
        {
            $conv=Convenio::findOne($id);
            $periodo=$conv->periodicidad;
            $plazo=$conv->plazos;
            $tasa=$conv->tasa;
            $porcentaje=($tasa/100);
            $monto=$model->monto;
            switch ($periodo)
            {
                case 1:
                    $ciclos=12;
                    break;
                case 2:
                    $ciclos=24;
                    break;
                case 3:
                    $ciclos=52;
                    break;
            }
            $totalfinal= $monto+$monto*$porcentaje*($plazo/$ciclos);
            $model->total_pagar=$totalfinal;
            
            if( $model->save()) {
                /*\yii\helpers\VarDumper::dump($totalfinal, 10, true);
                 die();*/
                
                   return $this->redirect(['view', 'id' => $model->id,'convenio'=>$conv]);
               }  
        }    
       
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    /**
     * Deletes an existing Credito model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * $perio->periodicidad=$conv->periodicidad;
        $plazo->plazos=$conv('plazos');
        $tasa->tasa=$conv('tasa');
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

   

    /**
     * Finds the Credito model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Credito the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Credito::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
