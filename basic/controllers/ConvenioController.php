<?php

namespace app\controllers;

use Yii;
use app\models\Convenio;
use app\models\ConvenioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Credito;


/**
 * ConvenioController implements the CRUD actions for Convenio model.
 */
class ConvenioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Convenio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConvenioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Convenio model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    { 
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Convenio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Convenio();
        $model->estatus=1;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
           
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Convenio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Convenio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->updateAttributes(['estatus'=>0 ,]);
        //mensaje
        Yii::$app->session->setFlash('success', 'Estatus actualizado con exito');
        return $this->redirect(['index']);
    }

    public function actionLists($id)
    {
       $countEmpresa= Empresas::find()
        ->where(['id'=>$id])
        ->count();  
        
        $empresas = Empresas::find()
        ->where(['id'=>$id])
        ->all();  
        
        if($countEmpresa > 0)
        {
            foreach($empresas as $empresas)
            {
                echo"<option value='".$empresas->id."'>".$empresas->nombre."</option>";
            }
        }
        else
        {
            echo "<option>-</option>";
        }
        
    }

    /**
     * Finds the Convenio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Convenio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Convenio::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

   public function actionGetPeriodicidad($id)
    {
       $total = Convenio::find()->where(['periodicidad'=>$id])->count();
        
       $periodicidad = Convenio::find()->where(['periodicidad'=>$id])->all();

       if($total > 0)
       {
           foreach($periodicidad as $key => $value)
           {
               echo "<option value='". $value->plazos ."'>$value->plazos</option>";
           }
       }
       else{
           echo "<option></option>";
       }
    }

    public function actionGetConvenio($id)
    {
       $total = Convenio::find()->where(['empresa_id'=>$id])->count();
        
       $convenio = Convenio::find()->where(['empresa_id'=>$id])->all();

       if($total > 0)
       {
           foreach($convenio as $key => $value)
           {
               echo "<option value='". $value->id ."'>$value->nombre</option>";
           }
       }
       else{
           echo "<option></option>";
       }
    }

    }

