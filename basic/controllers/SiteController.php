<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;
use app\models\Country;
use yii\filters\HttpBasicAuth;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;


class SiteController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];

        return[
            [
                'class'=>'yii\filters\HttpCache',
                'only'=>['index','view'],
                'lastModified'=>function($action,$params){
                    $q=new \yii\db\Query();
                    return $q->from('user')->max('updated_at');
                },
            ],
        ];

        return[
            'access'=>[
                'class'=>AccessControl::className(),
                'only'=>['create','update'],
                'rules'=>[
                    //permitido para usuarios autenticados
                    [
                        'allow'=>true,
                        'roles'=>['@'],
                    ],
                    //todo lo demas se deniega por defecto
                ],
            ],
        ];

        return[
            'basicAuth'=>[
                'class'=>HttpBasicAuth::className(),
            ],
        ];

        return[
            [
                'bootstrap'=>[
                    [
                'class'=>ContentNegotiator::className(),
                'formats'=>[
                    'application/json'=>Response::FORMAT_JSON,
                    'application/xml'=>Response::FORMAT_XML,
                ],
                'languages'=>[
                    'en-US',
                    'de'
                ],
            ],
        ],
    ],
];

        return[
            [
                'class'=>HttpCache::className(),
                'only'=>['index'],
                'lastModified'=>function($action,$params){
                    $q = new\yii\db\Query();
                    return $q->from('user')->max('update_at');
                },
            ],
        ];

        return[
            'pageCache'=>[
                'class'=>PageCache::className(),
                'only'=>['index'],
                'duration'=>60,
                'dependency'=>[
                    'class'=>DbDependency::className(),
                    'sql'=>'SELECT COUNT(*) FROM post',
                ],
                'variations'=>[
                    \yii::$app->language,
                ]
                ],
            ];
        
        return[
            'verbs'=>[
                'class'=>VerbFilter::className(),
                'action'=>[
                    'index'=>['get'],
                    'view'=>['get'],
                    'create'=>['get','post'],
                    'update'=>['get','put','post'],
                    'delete'=>['post','delete'],
                ],
            ],
        ];

        return ArrayHelper::merge([
            [
            'class'=> Cors::className(),
            'cors'=>[
                'Origin'=>['http://www.myserver.net'],
                'Access-Control-Request-Method'=>['GET','HEAD','OPTIONS'],
            ],
            'actions'=>[
                'login'=>[
                    'Access-Control-Allow-Credentials'=>true,
                ]
            ]
        ],
    ], parent::behaviors());

}

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /* Crear acción para responder peticiones(request)*/
    public function actionSay($message = 'Hola')
    {
        return $this->render('say',['message'=>$message]);
    }

    /*Creando Accion */
    public function actionEntry()
    {
        $model = new EntryForm;

        if($model -> load(Yii::$app->request->post()) && $model->validate())
        {
            //Validar datos recibidos en el modelo
            //aqui se hace algo significativo con el modelo
            return $this->render('entry-confirm',['model' => $model]);
        }else{
            //La pagina es mostrada inicialmente o hay algun eroor de validacion
            return $this->render('entry',['model'=>$model]);
        }
    }

    
}
