<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form=ActiveForm::begin(['id'=>'login-fomr', 'options'=>['class'=>'form_horizontal'],])?>

<?= $form->field($model, 'username')?>
<?= $form->field($model, 'password')->passwordInput() ?>

<div class="form-group">
  <div class="col-lg-offset-1 col-lg-11">
    <?= Html::submitButton('login',['class'=>'btn btn-primary']) ?>
  </div>  
</div>

<?php Activeform::end() ?>