<?php

use yii\db\Migration;

/**
 * Class m201203_193849_migracion2
 */
class m201203_193849_migracion2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201203_193849_migracion2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201203_193849_migracion2 cannot be reverted.\n";

        return false;
    }
    */
}
