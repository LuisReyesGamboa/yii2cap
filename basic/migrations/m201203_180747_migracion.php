<?php

use yii\db\Migration;

/**
 * Class m201203_180747_migracion
 */
class m201203_180747_migracion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Clientes',
        ['id'=>$this->primarykey(),
         'nombre'=>$this->string()->notNull(),
         'ap_paterno'=>$this->string()->notNull(),
         'ap_materno'=>$this->string()->notNull(),
         'fecha_nacimiento'=>$this->date()->notNull(),
         'rfc'=>$this->string()->notNull(),
         'estatus'=>$this->integer()->notNull(),
         'empresa_id'=>$this->integer()->notNull(),
         'fecha_alta'=>$this->datetime()->notNull(),
         'fecha_actualizacion'=>$this->datetime()->notNull()
       ]);

       $this->createTable('Empresa',
       ['id'=>$this->primaryKey(),
        'nombre'=>$this->string()->notNull(),
        'estatus'=>$this->integer()->notnull(),
        'fecha_alta'=>$this->datetime()->notNull(),
        'fecha_actualizacion'=>$this->datetime()->notNull()
        ]);
        
        $this->createTable('Convenio',
        ['id'=>$this->primaryKey(),
         'empresa_id'=>$this->integer()->notNull(),
         'nombre'=>$this->string()->notNull(),
         'plazos'=>$this->integer()->notNull(),
         'periodicidad'=>$this->integer()->notNull(),
         'tasa'=>$this->decimal(2,2)->notNull(),
         'estatus'=>$this->integer()->notNull(),
         'fecha_alta'=>$this->datetime()->notNull(),
         'fecha_actualizacion'=>$this->datetime()->notNull()
         ]);

         $this->createIndex(
          'idx-clientes-empresa_id',//nombre del index
          'Clientes',//tabla
          'empresa_id'//columna
      );
  
        $this->addForeignKey(
            'fk-clientes-empresa_id', //nombre de la fk
            'Clientes', //tabla 
            'empresa_id', //columna
            'Empresa', //tabla referencia
            'id',//columna referencia
            'CASCADE');

         $this->createIndex(
             'idx-convenio-empresa_id',//nombre del index
             'Convenio',//tabla
             'empresa_id'//columna
         );

         $this->addForeignKey(
             'fk-convenio-empresa_id', //nombre de la fk
             'Convenio', //tabla 
             'empresa_id', //columna
             'Empresa', //tabla referencia
             'id',//columna referencia
             'CASCADE');
         
         $this->createTable('Credito',
         ['id'=>$this->primaryKey(),
          'cliente_id'=>$this->integer()->notNull(),
          'convenio_id'=>$this->integer()->notNull(),
          'folio'=>$this->string()->notNull(),
          'monto'=>$this->decimal(10,2)->notNull(),
          'plazos'=>$this->integer()->notNull(),
          'peropdicidad'=>$this->integer()->notNull(),
          'tasa'=>$this->decimal(2,2)->notNull(),
          'descuento'=>$this->decimal(10,2)->notNull(),
          'total_pagar'=>$this->decimal(10,2)->notNull(),
          'fecha_alta'=>$this->datetime()->notNull(),
          'fecha_actualizacion'=>$this->datetime()->notNull()
          ]);

          $this->createIndex(
            'idx-credito-cliente_id',
            'Credito',
            'cliente_id');

          $this->addForeignKey(
            'fk-credito-cliente_id',
            'Credito',
            'cliente_id',
            'Clientes',
            'id',
            'CASCADE');

          $this->createIndex(
            'idx-credito-convenio_id',
            'Credito',
            'convenio_id');
          
          $this->addForeignKey(
            'fk-credito-convenio_id',
            'Credito',
            'convenio_id',
            'Convenio',
            'id',
            'CASCADE');

        $this->createTable('Credito_Pago',
         ['id'=>$this->primaryKey(),
          'credito_id'=>$this->integer()->notNull(),
          'numero_pago'=>$this->integer()->notNull(),
          'monto'=>$this->decimal(10,2)->notNull(),
          'fecha_pago'=>$this->datetime()->notNull(),
          'estatus'=>$this->integer()->notNull(),
          'fecha_alta'=>$this->datetime()->notNull(),
          'fecha_actualizacion'=>$this->datetime()->notNull()
          ]);

          $this->createIndex(
            'idx-credito-credito_id',//nombre del index
            'Credito_Pago',//tabla
            'credito_id'//columna
        );
    
          $this->addForeignKey(
              'fk-credito-credito_id', //nombre de la fk
              'Credito_Pago', //tabla 
              'credito_id', //columna
              'Credito', //tabla referencia
              'id',//columna referencia
              'CASCADE');
  

     }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201203_180747_migracion cannot be reverted.\n";

        $this->dropTable('Cliente');
        $this->dropTable('Empresa');
        $this->dropTable('Convenio');
        $this->dropTable('Credito');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201203_180747_migracion cannot be reverted.\n";

        return false;
    }
    */
}
