<?php

namespace app\modules\forum;
 
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        $this->params['foo']='bar';
        //... otro codigo de inicializacion...
    }

    /*Cuando el metodo init contiene mucho codigo
    parent::init();
    //Inicializa el modulo con la configuracion cargada desde
    //config.php
    \Yii::configure($this, require __DIR__ . '/config.php');
    */

    public function init1()
    {
        parent::init();

        $this->modules=[
            'admin'=>[
                //debe considerarse usar un nombre de espacios más corto
                'class'=>'app\modules\forum\modules\admin\Module',
            ],
        ];
    }
}
