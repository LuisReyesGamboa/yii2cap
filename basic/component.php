<?php
 namespace yii\components\MyClass;

 use yii\base\BaseObject;

 class MyClass extends BaseObject
 {
    public $prop1;
    public $prop2;

    public function __construct($param1, $param2, $config =[])
    {
        //..inicializacion antes de la configuracion esta siendo
        //aplicada

       parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        // ... inicialización después de la configuración esta siendo aplicada
    }
}
