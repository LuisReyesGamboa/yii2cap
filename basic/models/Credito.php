<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Credito".
 *
 * @property int $id
 * @property int $cliente_id
 * @property int $convenio_id
 * @property string $folio
 * @property float $monto
 * @property int $plazos
 * @property int $peropdicidad
 * @property float $tasa
 * @property float $descuento
 * @property float $total_pagar
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Clientes $cliente
 * @property Convenio $convenio
 */
class Credito extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Credito';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id', 'convenio_id', 'folio', 'monto', 'descuento'], 'required'],
            [['cliente_id', 'convenio_id' ],'integer'],
            [['monto', 'descuento', 'total_pagar'], 'number'],
            [['fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['folio'], 'string', 'max' => 255],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cliente_id' => 'id']],
            [['convenio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Convenio::className(), 'targetAttribute' => ['convenio_id' => 'id']],
            [['folio'],'unique'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente_id' => 'Cliente ID',
            'convenio_id' => 'Convenio ID',
            'folio' => 'Folio',
            'monto' => 'Monto',
            'descuento' => 'Descuento',
            'total_pagar' => 'Total Pagar',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    public function behaviors()
    {
        return[
            ['class'=>'yii\behaviors\TimestampBehavior',
             'createdAtAttribute'=>'fecha_alta',
             'updatedAtAttribute'=>'fecha_actualizacion',
             'value'=> new \yii\db\Expression('NOW()'),
        ]
        
        ];
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Convenio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConvenio()
    {
        return $this->hasOne(Convenio::className(), ['id' => 'convenio_id']);
    }
}
