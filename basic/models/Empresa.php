<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Empresa".
 *
 * @property int $id
 * @property string $nombre
 * @property int $estatus
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Convenio[] $convenios
 */
class Empresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'estatus', ], 'required'],
            [['estatus'], 'integer'],
            [['fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['nombre'], 'string', 'max' => 255],
            [['nombre'],'unique'],
            [['estatus'],'default','value'=>1]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'estatus' => 'Estatus',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    public function behaviors()
    {
        return[
            ['class'=>'yii\behaviors\TimestampBehavior',
             'createdAtAttribute'=>'fecha_alta',
             'updatedAtAttribute'=>'fecha_actualizacion',
             'value'=> new \yii\db\Expression('NOW()'),
            ]
        ];
    }

    /**
     * Gets query for [[Convenios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConvenios()
    {
        return $this->hasMany(Convenio::className(), ['empresa_id' => 'id']);
    }
}
