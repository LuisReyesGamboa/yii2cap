<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CreditoPago;

/**
 * CreditoPagoSearch represents the model behind the search form of `app\models\CreditoPago`.
 */
class CreditoPagoSearch extends CreditoPago
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'credito_id', 'numero_pago', 'estatus'], 'integer'],
            [['monto'], 'number'],
            [['fecha_pago', 'fecha_alta', 'fecha_actualizacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CreditoPago::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'credito_id' => $this->credito_id,
            'numero_pago' => $this->numero_pago,
            'monto' => $this->monto,
            'fecha_pago' => $this->fecha_pago,
            'estatus' => $this->estatus,
            'fecha_alta' => $this->fecha_alta,
            'fecha_actualizacion' => $this->fecha_actualizacion,
        ]);

        return $dataProvider;
    }
}
