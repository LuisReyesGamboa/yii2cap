<?php

namespace app\models;

use yii;
use yii\base\Model;

/**
 * This is the model class for table "Clientes".
 *
 * @property int $id
 * @property string $nombre
 * @property string $ap_paterno
 * @property string $ap_materno
 * @property string $fecha_nacimiento
 * @property string $rfc
 * @property int $estatus
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Credito[] $creditos
 */

class valores extends use Model 
 {
    public $edadmin;

 }

 public function rules()
 {
     return[
         ['fecha_nacimiento','validaredad']
     ];
 }

 public function validaredad($attribute,$params)
 {
       //edad min
       $edadmin = new DateTime();
       $edadmin->sub(new DateInterval('P18Y'));

       return[
           ['fecha_nacimiento','age','min'=>$edadmin,'tooBig'=>'Debes ser mayor a 18 años']
       ];
 }