<?php

namespace app\models;

use Yii;

use yii\validators\RangeValidator;


/**
 * This is the model class for table "Convenio".
 *
 * @property int $id
 * @property int $empresa_id
 * @property string $nombre
 * @property int $plazos
 * @property int $periodicidad
 * @property float $tasa
 * @property int $estatus
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Empresa $empresa
 * @property Credito[] $creditos
 */
class Convenio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Convenio';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id','nombre', 'periodicidad','plazos','tasa','estatus'], 'required'],
            [['empresa_id', 'periodicidad', 'estatus'], 'integer'],
            [['tasa'], 'number'],
            [['fecha_alta', 'fecha_actualizacion','periodicidad'], 'safe'],
            [['nombre'], 'string', 'max' => 255],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['nombre'],'unique'],
            ['estatus','default','value'=>1],
            ['periodicidad','in','range'=>['1','2','3']],
            ['tasa','in','range'=>['2.0','4.8']],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa ID',
            'nombre' => 'Nombre',
            'plazos' => 'Plazos',
            'periodicidad' => 'Periodicidad',
            'tasa' => 'Tasa',
            'estatus' => 'Estatus',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    public function behaviors()
    {
        return[
            ['class'=>'yii\behaviors\TimestampBehavior',
             'createdAtAttribute'=>'fecha_alta',
             'updatedAtAttribute'=>'fecha_actualizacion',
             'value'=> new \yii\db\Expression('NOW()'),
            ]
        ];

    }

    /**
     * Gets query for [[Empresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * Gets query for [[Creditos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreditos()
    {
        return $this->hasMany(Credito::className(), ['convenio_id' => 'id']);
    }
}
