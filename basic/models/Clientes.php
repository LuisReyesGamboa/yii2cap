<?php

namespace app\models;

use Yii;
use DateTime;
use DateInterval;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "Clientes".
 *
 * @property int $id
 * @property string $nombre
 * @property string $ap_paterno
 * @property string $ap_materno
 * @property string $fecha_nacimiento
 * @property string $rfc
 * @property int $estatus
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Credito[] $creditos
 */

class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
 
    public static function tableName()
    {

        return 'Clientes';
    }
    /**
     * {@inheritdoc}
     */
    const SCENARIO_SPECIAL = 'special';

    public function scenarios()
    {   
        $scenarios= parent::scenarios();
        $scenarios[self::SCENARIO_SPECIAL]=['estatus'];
        return $scenarios;
    }
    public function rules()
    {
        return [
            [['nombre', 'ap_paterno', 'ap_materno','rfc'], 'required'],
            [[ 'fecha_nacimiento','fecha_alta','fecha_actualizacion','estatus','empresa_id'],'safe'],
            [['estatus'], 'integer'],
            ['estatus','default','value'=>1],
           // [['estatus'],'required','on'=>self::SCENARIO_SPECIAL],
            [['nombre', 'ap_paterno', 'ap_materno', 'rfc'], 'string', 'max' => 255],
            [['nombre', 'ap_paterno', 'ap_materno'],'unique'],
            [['rfc'],'unique'],
        ];
    }

     /* {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ap_paterno' => 'Ap Paterno',
            'ap_materno' => 'Ap Materno',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'rfc' => 'Rfc',
            'estatus' => 'Estatus',
            'empresa' => 'Empresa_id',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    public function behaviors()
    {
        return[
            ['class'=>'yii\behaviors\TimestampBehavior',
             'createdAtAttribute'=>'fecha_alta',
             'updatedAtAttribute'=>'fecha_actualizacion',
             'value'=> new \yii\db\Expression('NOW()'),
        ]
        
        ];
    }

    /**
     * Gets query for [[Creditos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreditos()
    {
        return $this->hasMany(Credito::className(), ['cliente_id' => 'id']);
    }
}
