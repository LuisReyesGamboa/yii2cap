<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Credito_Pago".
 *
 * @property int $id
 * @property int $credito_id
 * @property int $numero_pago
 * @property float $monto
 * @property string $fecha_pago
 * @property int $estatus
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Credito $credito
 */
class CreditoPago extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Credito_Pago';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['credito_id', 'numero_pago', 'monto', 'fecha_pago', 'estatus', 'fecha_alta', 'fecha_actualizacion'], 'required'],
            [['credito_id', 'numero_pago', 'estatus'], 'integer'],
            [['monto'], 'number'],
            [['fecha_pago', 'fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['credito_id'], 'exist', 'skipOnError' => true, 'targetClass' => Credito::className(), 'targetAttribute' => ['credito_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'credito_id' => 'Credito ID',
            'numero_pago' => 'Numero Pago',
            'monto' => 'Monto',
            'fecha_pago' => 'Fecha Pago',
            'estatus' => 'Estatus',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    /**
     * Gets query for [[Credito]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCredito()
    {
        return $this->hasOne(Credito::className(), ['id' => 'credito_id']);
    }
}
