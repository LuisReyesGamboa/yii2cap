<?php 

use yii\data\ActiveDataProvider;

$query = Post::find()->where(['state_id'=>1]);

$provider = new ActiveDataProvider([
    'query'=>$query,
    'pagintaion'=>[ 'pageSize' => 10,],
    'sort'=>['defaultOrder'=>[ 'created_at'=>SORT_DESC,
                                'title'=>SORT-ASC,]],
]);

//Duevleve un array de objetos Post
$posts=$provider->getModels();