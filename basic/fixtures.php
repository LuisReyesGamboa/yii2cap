<?php

namespace app\test\fixtures;

use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture 
{
    public $modelClass= 'app\models\User';
}

class UserProfileFixture extends ActiveFixture 
{
    public $modelClass = 'app\models\UserProfile';
    public $depends = ['app\tests\fictures\UserFIxture'];
}