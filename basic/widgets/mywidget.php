<?php

namespace myname\mywidget;

use yii\base\BootstrapInterface;
use yii\base\Application;

class MyBootstrapClasa implements BootstrapInterface{
    public function bootstrap($app)
    {
        $app->on(Application::EVENT_BEFORE_REQUEST, function()
        { 
            //do something here
        });
    }
}